abstract class Animal(weight: Double, age: Int) {
    abstract fun doSomthing()
}

class Dog(var weight: Double, var age: Int, var race: String) : Animal(weight, age) {
    override fun doSomthing() {
        print("Woow woow")
    }
}

fun main(args: Array<String>) {
    var myDog = Dog(3.0, 12, "Yorkshire terrier")
    myDog.doSomthing()
}
